function createNewUser() {
  firstName = prompt("Введіть ваше імя");
  secondName = prompt("Введіть  ваше прізвище");
  birthday = prompt("Ведіть дату нарождення у форматі dd.mm.yyyy");
  let part = birthday.split(".").reverse()
  newUser = {
    firstName,
    secondName,
    birthday,
    getLogin: function () {
      let firstLetter = firstName.slice(0, 1).toLowerCase();
      return firstLetter + secondName.toLowerCase();
    },
    getAge: function () {
      today = new Date();
      birthday = new Date(part[0],part[1],part[2]);
      // year part[2], monthIndexpart[], day
      countAge = today.getFullYear() - birthday.getFullYear();
      if (
        today.getMonth() < birthday.getMonth() ||
        (today.getMonth() === birthday.getMonth() &&
          today.getDate() < birthday.getDate())
      ) {
        countAge--;
      }
      return countAge;
    },
    getPassword: function () {
      return (
        firstName.slice(0, 1).toUpperCase() +
        secondName.toLowerCase() +
        birthday.getFullYear()
      );
    },
  };
  return newUser;
}
let user = createNewUser();
console.log(user.getLogin());
console.log(user.getAge());
console.log(user.getPassword());
